package handler

import (
	"encoding/json"
	"net/http"
	"net/url"
	"time"

	"github.com/gorilla/mux"
	idencoder "gitlab.com/malice/go-url-shortener/pkg/IDencoder"
	"gitlab.com/malice/go-url-shortener/pkg/urlencoderdecoder"
	"gitlab.com/malice/go-url-shortener/storage/dao"
	"gitlab.com/malice/go-url-shortener/storage/dto"
)

func New(shortUrlDao dao.ShortURLDao, statsDao dao.URLStatsDao) http.Handler {
	router := mux.NewRouter()
	h := handler{
		urlEncoder: urlencoderdecoder.New(idencoder.New(), shortUrlDao),
		statsDao:   statsDao,
	}
	router.HandleFunc("/encode", h.shorten).Methods("POST")
	router.HandleFunc("/ping", h.Ping).Methods("GET")
	router.HandleFunc("/stats", h.Stats).Methods("GET")
	router.HandleFunc("/{encodedUrl}", h.shortenedUrlHandler).Methods("GET")
	return router
}

type handler struct {
	urlEncoder urlencoderdecoder.URLEncoderDecoder
	statsDao   dao.URLStatsDao
}

func (h handler) shorten(w http.ResponseWriter, req *http.Request) {
	var input struct{ URL string }
	if err := json.NewDecoder(req.Body).Decode(&input); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	inputUrl, err := url.Parse(input.URL)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	if inputUrl.Hostname() == "" {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	if inputUrl.Scheme == "" {
		inputUrl.Scheme = "http"
	}

	encodedUrl, _, err := h.urlEncoder.Encode(inputUrl)

	if err != nil || encodedUrl == nil {
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}
	resp := struct {
		EncodedUrl string `json:"shortUrl"`
	}{encodedUrl.String()}
	respJson, err := json.Marshal(resp)
	if err != nil {
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}
	w.Write(respJson)
}

func (h handler) shortenedUrlHandler(w http.ResponseWriter, req *http.Request) {
	inurl, _ := url.Parse(req.RequestURI)
	decodedUrl, decodedID, err := h.urlEncoder.Decode(inurl)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	go h.UpdateAccessRecord(decodedID)
	http.Redirect(w, req, decodedUrl.String(), http.StatusMovedPermanently)
}
func (h handler) UpdateAccessRecord(id int64) {
	h.statsDao.Save(&dto.URLStatRecord{ShortURLID: id})

}

func (h handler) Stats(w http.ResponseWriter, req *http.Request) {

	queryURL := req.URL.Query().Get("url")
	queryTime := req.URL.Query().Get("time")
	var timeFrom time.Time
	switch queryTime {
	case "d":
		timeFrom = time.Now().Add(-time.Hour * 24)
	case "w":
		timeFrom = time.Now().Add(-time.Hour * 24 * 7)
	default:
		timeFrom = time.Time{}
	}
	inurl, _ := url.Parse(queryURL)
	_, decodedID, err := h.urlEncoder.Decode(inurl)

	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	stats, err := h.statsDao.LoadStats(decodedID, timeFrom)
	var statsJson struct {
		Count int64 `json:"count"`
	}
	statsJson.Count = stats.Count
	resBody, err := json.Marshal(statsJson)
	w.Write(resBody)
}

func (h handler) Ping(w http.ResponseWriter, req *http.Request) {
	w.Write([]byte("Pong"))
}
