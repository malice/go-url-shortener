package handler

import (
	"bytes"
	"fmt"
	"net/http/httptest"
	"net/url"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/malice/go-url-shortener/pkg/urlencoderdecoder"
	"gitlab.com/malice/go-url-shortener/storage/dao"
	"gitlab.com/malice/go-url-shortener/storage/dto"
)

func TestEncoderHandler(t *testing.T) {
	testCases := []struct {
		InputURL   string
		EncodedID  string
		OutputURL  string
		EncodedURL string
		EncoderErr error
		StatusCode int
	}{
		{
			"http://www.google.com",
			"ABCD",
			`{"shortUrl":"locahost:8080/ABCD"}`,
			"locahost:8080/ABCD",
			nil,
			200,
		},
		{
			"http://www.google.",
			"ABCD",
			`{"shortUrl":"locahost:8080/ABCD"}`,
			"locahost:8080/ABCD",
			nil,
			200,
		},
		{
			"http://www.google.com",
			"ABCD",
			"",
			"",
			fmt.Errorf("error"),
			503,
		},
	}
	for _, testCase := range testCases {
		resp := httptest.NewRecorder()
		req := httptest.NewRequest("POST", "localhost:8080/encode", bytes.NewBufferString(`{"URL":"`+testCase.InputURL+`"}`))
		mockUrlEncoder := &urlencoderdecoder.MockURLEncoderDecoder{}
		InputURL, _ := url.Parse(testCase.InputURL)
		EncodedURL, _ := url.Parse(testCase.EncodedURL)
		mockUrlEncoder.On("Encode", InputURL).Return(EncodedURL, testCase.EncodedID, testCase.EncoderErr)
		testHandler := handler{
			urlEncoder: mockUrlEncoder,
		}
		testHandler.shorten(resp, req)
		assert.Equal(t, testCase.OutputURL, resp.Body.String())
		assert.Equal(t, testCase.StatusCode, resp.Result().StatusCode)

	}
}
func TestDecoderHandler(t *testing.T) {
	testCases := []struct {
		InputURL   string
		DecodedID  int64
		OutputURL  string
		DecoderErr error
		StatusCode int
		Response   string
	}{
		{

			"locahost:8080/ABCD",
			0,
			"http://www.google.com",
			nil,
			301,
			"<a href=\"http://www.google.com\">Moved Permanently</a>.\n\n",
		},
		{
			"locahost:8080/ABCD",
			0,
			"http://www.google.",
			nil,
			301,
			"<a href=\"http://www.google.\">Moved Permanently</a>.\n\n",
		},
		{

			"locahost:8080/ABCD",
			0,
			"",
			fmt.Errorf("error"),
			404,
			"",
		},
	}
	for _, testCase := range testCases {
		resp := httptest.NewRecorder()
		req := httptest.NewRequest("GET", testCase.InputURL, nil)
		mockUrlEncoder := &urlencoderdecoder.MockURLEncoderDecoder{}
		InputURL, _ := url.Parse(testCase.InputURL)
		OutputURL, _ := url.Parse(testCase.OutputURL)
		mockUrlEncoder.On("Decode", InputURL).Return(OutputURL, testCase.DecodedID, testCase.DecoderErr)
		mockStatsDao := &dao.MockURLStatsDao{}
		mockStatsDao.On("Save", &dto.URLStatRecord{ShortURLID: testCase.DecodedID}).Return(int64(0), nil)
		testHandler := handler{
			urlEncoder: mockUrlEncoder,
			statsDao:   mockStatsDao,
		}
		testHandler.shortenedUrlHandler(resp, req)
		assert.Equal(t, testCase.Response, resp.Body.String())
		assert.Equal(t, testCase.StatusCode, resp.Result().StatusCode)

	}
}
func TestInfoHandler(t *testing.T) {
	testCases := []struct {
		InputURL   string
		OutputURL  string
		DecodedID  int64
		DecoderErr error
		readCount  int64
		StatusCode int
		Response   string
	}{
		{

			"http://localhost:8080/stats?url=http://localhost:8080/GQZDSMBTGA4DEMI&time=w",
			"http://localhost:8080/GQZDSMBTGA4DEMI",
			1,
			nil,
			4,
			200,
			`{"count":4}`,
		},
	}
	for _, testCase := range testCases {
		resp := httptest.NewRecorder()
		req := httptest.NewRequest("GET", testCase.InputURL, nil)
		mockUrlEncoder := &urlencoderdecoder.MockURLEncoderDecoder{}
		//InputURL, _ := url.Parse(testCase.InputURL)
		OutputURL, _ := url.Parse(testCase.OutputURL)
		mockUrlEncoder.On("Decode", OutputURL).Return(OutputURL, testCase.DecodedID, testCase.DecoderErr)
		mockStatsDao := &dao.MockURLStatsDao{}
		mockStatsDao.On("LoadStats", mock.Anything, mock.Anything).Return(&dto.URLStats{Count: testCase.readCount}, nil)
		testHandler := handler{
			urlEncoder: mockUrlEncoder,
			statsDao:   mockStatsDao,
		}
		testHandler.Stats(resp, req)
		assert.Equal(t, testCase.Response, resp.Body.String())
		assert.Equal(t, testCase.StatusCode, resp.Result().StatusCode)

	}
}
