package urlencoderdecoder

import (
	"net/url"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	idencoder "gitlab.com/malice/go-url-shortener/pkg/IDencoder"
	"gitlab.com/malice/go-url-shortener/storage/dao"
	"gitlab.com/malice/go-url-shortener/storage/dto"
)

//happyPath
func TestURLEncoder(t *testing.T) {
	testCases := []struct {
		InputURL         string
		DaoReturns       int64
		IdEncoderReturns string
		OutputURL        string
	}{
		{
			"http://www.google.com",
			5,
			"ABCDEFG",
			"http://localhost:8080/ABCDEFG",
		},
		{
			"http://localhost:8080",
			6,
			"XYZ",
			"http://localhost:8080/XYZ",
		},
	}

	for _, testCase := range testCases {
		mockShortURLDao := &dao.MockShortURLDao{}
		mockShortURLDao.On("Save", mock.Anything).Return(testCase.DaoReturns, nil)
		mockIDEncoder := &idencoder.MockEncoderDecoder{}
		mockIDEncoder.On("Encode", testCase.DaoReturns).Return(testCase.IdEncoderReturns)
		URLEncoder := New(mockIDEncoder, mockShortURLDao)
		inputURL, _ := url.Parse(testCase.InputURL)

		encodedURL, _, err := URLEncoder.Encode(inputURL)
		assert.Nil(t, err)
		assert.Equal(t, testCase.OutputURL, encodedURL.String())
	}
	assert.True(t, true)
}
func TestURLDecoder(t *testing.T) {
	testCases := []struct {
		InputURL         string
		encodedID        string
		IdDecoderReturns int64
		DaoReturns       *dto.ShortRecord
		OutputURL        string
	}{
		{
			"http://localhost:8080/ABCDEFG",
			"ABCDEFG",
			5,
			&dto.ShortRecord{URL: "http://www.google.com", ID: 5},
			"http://www.google.com",
		},
		{
			"http://loalcalhost:8080/XYZ",
			"XYZ",
			5,
			&dto.ShortRecord{URL: "http://localhost:8080", ID: 5},
			"http://localhost:8080",
		},
	}

	for _, testCase := range testCases {
		mockIDEncoder := &idencoder.MockEncoderDecoder{}
		mockIDEncoder.On("Decode", testCase.encodedID).Return(testCase.IdDecoderReturns)
		mockShortURLDao := &dao.MockShortURLDao{}
		mockShortURLDao.On("LoadByID", mock.Anything).Return(testCase.DaoReturns, nil)
		URLEncoder := New(mockIDEncoder, mockShortURLDao)
		inputURL, _ := url.Parse(testCase.InputURL)

		decodedURL, _, err := URLEncoder.Decode(inputURL)
		assert.Nil(t, err)
		assert.Equal(t, testCase.OutputURL, decodedURL.String())
	}
	assert.True(t, true)
}

//OnDaoError
