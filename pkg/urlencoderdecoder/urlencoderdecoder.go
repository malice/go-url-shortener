package urlencoderdecoder

import (
	"net/url"
	"path"
	"strings"

	idencoder "gitlab.com/malice/go-url-shortener/pkg/IDencoder"
	"gitlab.com/malice/go-url-shortener/storage/dao"
	"gitlab.com/malice/go-url-shortener/storage/dto"
)

const baseUrl = "http://localhost:8080"

type URLEncoderDecoder interface {
	Encode(URL *url.URL) (*url.URL, string, error)
	Decode(URL *url.URL) (*url.URL, int64, error)
}

type implURLEncoderDecoder struct {
	idEncoder   idencoder.EncoderDecoder
	shortURLDao dao.ShortURLDao
}

func (t implURLEncoderDecoder) Encode(URL *url.URL) (*url.URL, string, error) {
	object := &dto.ShortRecord{
		URL: URL.String(),
	}
	id, err := t.shortURLDao.Save(object)
	if err != nil {
		return nil, "", err
	}
	encodedID := t.idEncoder.Encode(id)
	parsedUrl, err := url.Parse(baseUrl)
	if err != nil {
		return nil, "", err
	}
	parsedUrl.Path = path.Join(parsedUrl.Path, encodedID)
	return parsedUrl, encodedID, nil //url.Parse("http://shortgoogle.com")
}

func (t implURLEncoderDecoder) Decode(URL *url.URL) (*url.URL, int64, error) {
	encodedId := URL.Path
	recordID := t.idEncoder.Decode(strings.TrimLeft(encodedId, "/"))
	record, err := t.shortURLDao.LoadByID(recordID)
	if err != nil {
		return nil, recordID, err
	}
	parsedURl, err := url.Parse(record.URL)
	return parsedURl, recordID, err
}

func New(IDEncoder idencoder.EncoderDecoder, shortURLDao dao.ShortURLDao) URLEncoderDecoder {
	return implURLEncoderDecoder{
		idEncoder: IDEncoder, shortURLDao: shortURLDao,
	}
}
