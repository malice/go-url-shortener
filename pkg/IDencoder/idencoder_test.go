package idencoder

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestEncoder(t *testing.T) {
	testCases := []struct {
		ID        int64
		EncodedID string
	}{
		{ID: 1, EncodedID: "GIZTIMZVGU3TAMJS"},
		{ID: 100, EncodedID: "GEYTGMJXGA3TKNRY"},
		{ID: 0, EncodedID: "GIYDCOJSGE2DMNRZ"},
	}
	for _, testCase := range testCases {
		encoder := New()
		assert.Equal(t, encoder.Encode(testCase.ID), testCase.EncodedID)
		assert.Equal(t, encoder.Decode(encoder.Encode(testCase.ID)), testCase.ID)
	}
}
