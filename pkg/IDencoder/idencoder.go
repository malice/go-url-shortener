package idencoder

import (
	"fmt"
	"strconv"

	"encoding/base32"
)

const x = 173741789
const y = 324342343
const z = 2563210877
const y_1 = 760457502

type EncoderDecoder interface {
	Encode(ID int64) string
	Decode(str string) int64
}

type implEncoderDecoder struct {
}

func (t implEncoderDecoder) Encode(ID int64) string {
	newID := ((ID + x) * y) % z
	encodedID := base32.StdEncoding.EncodeToString([]byte(fmt.Sprintf("%d", newID)))
	return encodedID
}

func (t implEncoderDecoder) Decode(str string) int64 {
	s, _ := base32.StdEncoding.DecodeString(str)
	newID, _ := strconv.ParseInt(string(s), 10, 64)
	ID := (newID*y_1 - x) % z
	return ID
}

func New() EncoderDecoder {
	return implEncoderDecoder{}
}
