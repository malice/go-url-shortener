package dao

import (
	"database/sql"
	"fmt"

	"gitlab.com/malice/go-url-shortener/storage/dto"
)

const tableNameShortURL = "shorturl"

//mockery -inpkg ShortURLDao
type ShortURLDao interface {
	LoadByID(int64) (*dto.ShortRecord, error)
	Save(*dto.ShortRecord) (int64, error)
}

type serviceImpl struct {
	db *sql.DB
}

func (s *serviceImpl) LoadByID(ID int64) (*dto.ShortRecord, error) {
	var shortURL string
	err := s.db.QueryRow(fmt.Sprintf("Select id,url from %s where id=%d", tableNameShortURL, ID)).Scan(&ID, &shortURL)
	if err != nil {
		return nil, err
	}
	return &dto.ShortRecord{ID: ID, URL: shortURL}, nil
}

func (s *serviceImpl) Save(row *dto.ShortRecord) (int64, error) {
	var id int64
	stmt, err := s.db.Prepare(fmt.Sprintf("INSERT INTO %s (url) VALUES ($1) returning id", tableNameShortURL))
	if err != nil {
		return -1, err
	}
	if err = stmt.QueryRow(row.URL).Scan(&id); err != nil {
		return -1, err
	}
	if err != nil {
		return -1, err
	}
	return id, nil
}
func NewShortURLDao(db *sql.DB) ShortURLDao {
	return &serviceImpl{
		db: db,
	}
}
