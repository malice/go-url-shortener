package dao

import (
	"database/sql"
	"fmt"
	"time"

	"gitlab.com/malice/go-url-shortener/storage/dto"
)

const tableNameURLStats = "urlstats"

//mockery -inpkg ShortURLDao
type URLStatsDao interface {
	LoadByID(int64) (*dto.URLStatRecord, error)
	Save(*dto.URLStatRecord) (int64, error)
	LoadStats(ID int64, accessTimeAfter time.Time) (*dto.URLStats, error)
}

type uRLStatsServiceImpl struct {
	db *sql.DB
}

func (s *uRLStatsServiceImpl) LoadByID(ID int64) (*dto.URLStatRecord, error) {
	var shortURLID int64
	var accessTime time.Time
	s.db.QueryRow(fmt.Sprintf("Select id,shorturlid,time from %s where id=%d", tableNameURLStats, ID)).Scan(&ID, &shortURLID, &accessTime)
	return &dto.URLStatRecord{ID: ID, ShortURLID: shortURLID, AccessTime: accessTime}, nil
}

func (s *uRLStatsServiceImpl) LoadStats(ID int64, accessTimeAfter time.Time) (*dto.URLStats, error) {
	count := int64(0)
	s.db.QueryRow(fmt.Sprintf("Select count(id) from %s where shorturlid=%d and createdat > timestamp  '%s' ", tableNameURLStats, ID, accessTimeAfter.Round(0).Format("2006-01-02T15:04:05.000000Z"))).Scan(&count)
	return &dto.URLStats{Count: count}, nil
}
func (s *uRLStatsServiceImpl) Save(row *dto.URLStatRecord) (int64, error) {
	var id int64
	if row.AccessTime.IsZero() {
		row.AccessTime = time.Now()
	}
	stmt, err := s.db.Prepare(fmt.Sprintf("INSERT INTO %s (shorturlid, createdat) VALUES ($1,$2) returning id", tableNameURLStats))
	if err != nil {
		return -1, err
	}
	if err = stmt.QueryRow(row.ShortURLID, row.AccessTime).Scan(&id); err != nil {
		return -1, err
	}
	if err != nil {
		return -1, err
	}
	return id, nil
}
func NewURLDao(db *sql.DB) URLStatsDao {
	return &uRLStatsServiceImpl{
		db: db,
	}
}
