package dto

import "time"

type ShortRecord struct {
	ID  int64
	URL string
}

type URLStatRecord struct {
	ID         int64
	ShortURLID int64
	AccessTime time.Time
}
type URLStats struct {
	ShortURLID int64
	Count      int64
	Time       time.Time
}
