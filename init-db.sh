set -e

	#kj
psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE USER test WITH PASSWORD 'test123';
    GRANT ALL PRIVILEGES ON DATABASE postgres TO test;
    CREATE TABLE shorturl (id serial primary key , url text);
    GRANT ALL PRIVILEGES ON TABLE shorturl TO test;
	GRANT USAGE, SELECT ON SEQUENCE shorturl_id_seq TO test;
    CREATE TABLE urlstats (id serial primary key , shorturlid integer, createdat timestamp);
    GRANT ALL PRIVILEGES ON TABLE urlstats TO test;
	GRANT USAGE, SELECT ON SEQUENCE urlstats_id_seq TO test;
EOSQL
