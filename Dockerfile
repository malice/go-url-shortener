FROM golang:1.14.6-alpine3.12 as builder
COPY go.mod go.sum /go/src/gitlab.com/malice/go-url-shortener/
WORKDIR /go/src/gitlab.com/malice/go-url-shortener
RUN go mod download
COPY . /go/src/gitlab.com/malice/go-url-shortener/
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o build/go-url-shortener gitlab.com/malice/go-url-shortener

FROM alpine
RUN apk add --no-cache ca-certificates && update-ca-certificates
COPY --from=builder /go/src/gitlab.com/malice/go-url-shortener/build/go-url-shortener /usr/bin/go-url-shortener
EXPOSE 8080 8080
ENTRYPOINT ["/usr/bin/go-url-shortener"]
