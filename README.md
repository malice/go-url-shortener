PREREQUISITES
------------------
1. Docker
2. go >= 1.14



Assumptions
-----------------
1. Postgres Data stored on docker volumes
2. Urls missing schemes will default to http
3. No logging for the server is available.


Build
------
Clone the reposiitory
```
git clone https://gitlab.com/malice/go-url-shortener.git
cd go-url-shortener
```
Build and start docker containters
```
docker-compose up --build
```

To send Requests
1. Shorten a url
```
curl -X POST --header "Content-Type: application/json" localhost:8080/encode -d '{"URL":"https://www.yahoo.com"}'
```
2. Copy the url in response and paste it in your browser !!

3. TO get the stats about a shorturl
```
curl -X GET --header "Content-Type: application/json" "localhost:8080/stats?url={your short url above}&time=w"
```


