module gitlab.com/malice/go-url-shortener

go 1.13

require (
	github.com/catinello/base62 v0.0.0-20160325105823-e0daaeb631c9
	github.com/gorilla/mux v1.8.0
	github.com/lib/pq v1.8.0
	github.com/lytics/base62 v0.0.0-20180808010106-0ee4de5a5d6d
	github.com/stretchr/testify v1.6.1
)
