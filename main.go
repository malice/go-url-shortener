package main

import (
	"fmt"
	"net/http"
	"os"

	"gitlab.com/malice/go-url-shortener/handler"
	"gitlab.com/malice/go-url-shortener/storage/connection"
	"gitlab.com/malice/go-url-shortener/storage/dao"
)

type AppConfig struct {
	ServerPort       string
	ServerHost       string
	PostgresHost     string
	PostgresPort     int64
	PostgresUserName string
	PostgresPassword string
	PostgresDB       string
}

func getConfig() *AppConfig {
	env := os.Getenv("env")
	switch env {
	case "prd":
		return &AppConfig{}
	case "stg":
		return &AppConfig{}
	default:
		return &AppConfig{
			os.Getenv("SERVERPORT"),
			os.Getenv("SERVERHOST"),
			"host.docker.internal",
			5432,
			"test",
			os.Getenv("POSTGRES_PASSWORD"),
			"postgres",
		}

	}
}

func main() {
	config := getConfig()
	db := connection.Init(config.PostgresHost, config.PostgresPort, config.PostgresUserName, config.PostgresPassword, config.PostgresDB)
	urlRecordDao := dao.NewShortURLDao(db)
	urlStatsDao := dao.NewURLDao(db)
	server := &http.Server{
		Addr:    fmt.Sprintf("%s:%s", config.ServerHost, config.ServerPort),
		Handler: handler.New(urlRecordDao, urlStatsDao),
	}
	server.ListenAndServe()
}
